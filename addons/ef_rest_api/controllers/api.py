from odoo import http
from odoo.http import request, Response, JsonRequest
import datetime
import json
import requests
import base64
import logging
import calendar

_logger = logging.getLogger(__name__)


"""
Write down your key
Here is your new API key, use it instead of a password for RPC access. Your login is still necessary for interactive usage.

b83fff5323b5340158fab1d888a1a751e8f9f3db

"""


class SaleOrder(http.Controller):
    @http.route(['/api/so'], type='http', auth="user", website=False , redirect=None, csrf=False,sitemap=False ,method=['GET'])
    def index(self, **post):
        so = request.env['sale.order'].sudo().search([])
        _logger.warning(post)
        _logger.warning(so)
        headers = {'Content-Type': 'application/json'}
        body = str(so.read())

        _logger.warning(so.read())
        return Response(json.dumps(body, indent=4, separators=(". ", " = ")), headers=headers)


    @http.route(['/api/so/<int:so_id>'], type='http', auth="user", website=False , redirect=None, csrf=False,sitemap=False ,method=['GET'])
    def solist(self, so_id):
        so = request.env['sale.order'].sudo().search([('id','=',so_id)])
        _logger.warning(so)
        headers = {'Content-Type': 'application/json'}
        body = str(so.read())
        _logger.warning(so.read())
        return Response(json.dumps(body, indent=4, separators=(". ", " = ")), headers=headers)



    @http.route(['/api/so/<int:so_id>'], type='http', auth="user", website=False , redirect=None, csrf=False,sitemap=False ,method=['GET'])
    def solist(self, so_id):
        so = request.env['sale.order'].sudo().search([('id','=',so_id)])
        _logger.warning(so)
        headers = {'Content-Type': 'application/json'}
        body = str(so.read())
        _logger.warning(so.read())
        return Response(json.dumps(body, indent=4, separators=(". ", " = ")), headers=headers)

    
    @http.route(['/api/so/update/<int:so_id>'], type='json', auth="user", website=False , redirect=None, csrf=False,sitemap=False ,method=['POST','PUT'])
    def soupdate(self, so_id, **post):
        so = request.env['sale.order'].sudo().search([('id','=',so_id)])
        data= http.request.httprequest.data
        data_raw =  json.loads(data.decode('UTF-8'))
        update = so.write(data_raw)
        _logger.warning(data)
        _logger.warning(data_raw)
        return (so.name + " Telah Terupdate")

    @http.route(['/api/so/create'], type='json', auth="user", website=False , redirect=None, csrf=False,sitemap=False ,method=['POST'])
    def socreate(self, **post):
        data= http.request.httprequest.data
        data_raw =  json.loads(data.decode('UTF-8'))
        so = request.env['sale.order'].sudo().create(data_raw)
        return so.read()

    